# Project 19-Under-the-C

2D igrica. Korisnik pomera igraca. Komande za pomeranje igraca ce u nekim nivoima biti samo pomeranje levo-desno a u nekim nivoima ce imati opciju skoka u stilu platformer igrica. Cilj igrice je preci sve nivoe koji ce imati razlicite prepreke (razlicite vrste neprijatelja, sakupljanje odredjenih dobara koji mogu biti neophodni za zavrsetak nivoa ili sakupljanje odredjenih power up-ova koji bi igracu omogucili da dopre do delova nivoa do kojih inace ne bi uspeo...). Vrlo je moguce i dodavanje odredjene price u igricu ili u vidu teksta ili kroz kracu animaciju (ili oba), cilj price bi bio povezivanje svih nivoa u jednu celinu i davanje smisla cilju igrice.

## Developers

- [Angelina Jordanov, 422/2019](https://gitlab.com/Sogeking099)
- [Katarina Popovic, 361/2020](https://gitlab.com/katarina.popovic)
- [Irina Marko, 243/2017](https://gitlab.com/irinamarko)
- [Milana Novakovic, 467/2018](https://gitlab.com/milana98)
