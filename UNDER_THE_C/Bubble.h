#ifndef BUBBLE_H
#define BUBBLE_H

#include <QGraphicsPixmapItem>
#include <QObject>
class Bubble:public QObject, public QGraphicsPixmapItem{
    Q_OBJECT
public:
    static int i;
    Bubble();
    static int get_i();
public slots:
    void move();

};

#endif // BUBBLE_H
