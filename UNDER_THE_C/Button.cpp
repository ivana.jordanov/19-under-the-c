#include "Button.h"


Button::Button(QGraphicsItem *parent)
{}

void Button::mousePressEvent(QGraphicsSceneMouseEvent *event)
{
   emit clicked();
}

void Button::initialize(QString name, float scale, float width, float height)
{

    setPixmap(QPixmap(name));
    setScale(scale);
    setPos(width,height);
}
