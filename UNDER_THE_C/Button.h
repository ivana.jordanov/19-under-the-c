#ifndef BUTTON_H
#define BUTTON_H

#include <QGraphicsRectItem>
#include <QGraphicsSceneMouseEvent>
#include <QGraphicsPixmapItem>
#include <QGraphicsItem>
#include <QObject>
#include <QString>
class Button:public QObject, public QGraphicsPixmapItem{
  Q_OBJECT
public:
    Button(QGraphicsItem *parent=0);
    void mousePressEvent(QGraphicsSceneMouseEvent *event );
    void initialize(QString name, float scale, float width, float height);
signals:
    void clicked();
};



#endif // BUTTON_H
