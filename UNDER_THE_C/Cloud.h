#ifndef CLOUD_H
#define CLOUD_H
#include <QObject>
#include<QGraphicsPixmapItem>
class Cloud:public QObject, public QGraphicsPixmapItem{
public:
    Cloud(QGraphicsItem * parent=0);
    static double ubrzanjeCloud;
    static int brojac;
    int ID;
    int height();
};

#endif // CLOUD_H
