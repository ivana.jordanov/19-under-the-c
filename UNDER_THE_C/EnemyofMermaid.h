#ifndef ENEMYOFMERMAID_H
#define ENEMYOFMERMAID_H
#include <QGraphicsPixmapItem>
#include <QObject>
#include <QPainter>
class EnemyofMermaid:public QObject, public QGraphicsPixmapItem{
    Q_OBJECT
public:

    EnemyofMermaid(QGraphicsPixmapItem *parent=nullptr);
    static double speedUp;
    static int counter;
    int ID;


};
#endif // ENEMYOFMERMAID_H
