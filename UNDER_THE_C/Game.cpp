#include "Game.h"

Game::Game(QObject * parent):
    QObject(parent),
    current_level(0),
    timer(nullptr),
    view(nullptr),
    numberOfLifes(3)
{

}
Game::~Game()
{
    delete timer;
    delete view;
}

void Game::addLevel(LevelScene * level)
{
    levels.append(level);
}
// ideja, napraviti initialize tamo dodam sve nivoe i dodam
//jedan parametar koji pamti do kog sam nivoa stigla pa u play smenjujem po tome
void Game::onPlay()
{
    auto level = levels.at(current_level);

    if(level == nullptr)
        return;

    level->initialize();


      view->scene()->clear();
      view->viewport()->update();
      view->setScene(level);


    view->setScene(level);
    //view->setResizeAnchor(QGraphicsView::AnchorViewCenter);
    //view->resizeAnchor();
    view->show();


    view->setFixedSize(QSize(1280, 720));
    view->setHorizontalScrollBarPolicy(Qt::ScrollBarAlwaysOff);
    view->setVerticalScrollBarPolicy(Qt::ScrollBarAlwaysOff);


    timer = new QTimer();
    connect(timer, SIGNAL(timeout()), level, SLOT(update()));
    timer->start(50);

    //kad se emituje signal da je nivo zavrsen da pozovem funkciju
    //koja zove sledeci nivo
    connect(level, SIGNAL(lifeLost()), this, SLOT(onLifeLost()));
    connect(level, SIGNAL(lifeCollected()), this, SLOT(onLifeCollected));
    connect(level, SIGNAL(levelCompleted()), this, SLOT(onLevelCompleted));

    numberOfLifes = 3;

}

void Game::initialize()
{
    auto level1 = new LevelMimi();
    auto level2 = new LevelPlatform();
    auto level3 = new LevelMermaid();

    addLevel(level1);
    addLevel(level2);
    addLevel(level3);

    current_level = 0;

    view= new QGraphicsView();
    //view->setHorizontalScrollBarPolicy(Qt::ScrollBarAlwaysOff);
    //view->setVerticalScrollBarPolicy(Qt::ScrollBarAlwaysOff);

}

void Game::onLifeLost()
{
    numberOfLifes--;
    //Da li pozivati ovde desktrutore
    if(numberOfLifes <= 0){
        timer->stop();
        view->scene()->clear();
        return;
    }
    else{
        timer->stop();
        view->scene()->clear();

        auto level = levels.at(current_level);

        level->initialize();
        view->viewport()->update();

        timer->start(50);
    }
}
// trenutno u nivoima jos nisu obradjeni delovi koji treba da emituju
// signali koji bi aktivirali naredna dva slota, tako da kod ispod jos nije testiran
void Game::onLifeCollected()
{
    numberOfLifes++;
}

void Game::onLevelCompleted()
{
    qDebug()<<"Zavrsen nivo";

  /*  timer->stop();


    current_level++;

    // WARNING: ovaj slot ne treba pozivati za poslednji nivo igrice
    // ili ako se pozove za njega treba ga speciajlno obraditi

    auto level = levels.at(current_level);
    level->initialize();

    view->scene()->clear();
    view->viewport()->update();
    view->setScene(level);

    connect(timer, SIGNAL(timeout()), level, SLOT(update()));
    connect(level, SIGNAL(lifeLost()), this, SLOT(onLifeLost()));
    connect(level, SIGNAL(lifeCollected()), this, SLOT(onLifeCollected));
    connect(level, SIGNAL(levelCompleted()), this, SLOT(onLevelCompleted));

    timer->start();
*/
}

void Game::onExit()
{
    QApplication::closeAllWindows();
}
