#ifndef GAME_H
#define GAME_H

#include <QObject>

#include <QTimer>
#include <QList>
#include <QGraphicsView>
#include <QApplication>
#include <Levelscene.h>
//o ovome jos razmisliti
#include <MenuScene.h>
#include <LevelMimi.h>
#include <LevelPlatform.h>
#include <LevelMermaid.h>
#include <LevelPinky.h>

class Game: public QObject{
    Q_OBJECT
public:

    Game(QObject *parent = nullptr);
    ~Game();
    QGraphicsView * view;
    void addLevel(LevelScene*);
    void initialize();

public slots:
    void onPlay();
    void onLifeLost();
    void onLifeCollected();
    void onLevelCompleted();
    void onExit();

private:
    QList<LevelScene*> levels;
    int current_level;
    QTimer * timer; 
    int numberOfLifes;
};

#endif // GAME_H
