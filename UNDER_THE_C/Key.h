#ifndef KEY_H
#define KEY_H
#include <QGraphicsRectItem>
#include <QObject>

class Key:public QObject, public QGraphicsPixmapItem{
    Q_OBJECT
public:
    Key(QGraphicsItem * parent=0);
    static double ubrzanje_key;
    static int brojac;
    int ID;
    int height();

};
#endif // KEY_H


