#include "LevelMermaid.h"
#include "Bubble.h"
#include "PlayerMermaid.h"

LevelMermaid::LevelMermaid(QObject *parent):
    LevelScene(parent)
{}


void LevelMermaid::initialize()
{
    setSceneRect(0,0,1280,720);
    setBackgroundBrush(QPixmap(":/backgrounds/backgrounds/sea.jpg").scaled(1280,720));
    player= new PlayerMermaid();
    addItem(player);
    setFocus(Qt::TabFocusReason);
    player->setFocus();
    player->setPos(0,height()/2);
    timeCounter=0;
    //ovo ce nasldjivati kada se budu spojili nivoi
    pScore = new Score();
    addItem(pScore);
    //ovo ce nasldjivati kada se budu spojili nivoi
    pHealth = new Health();
    pHealth->setPos(pHealth->x(), pHealth->y()+25);
    addItem(pHealth);
    sounds();

}

void LevelMermaid::keyPressEvent(QKeyEvent *event)
{
    if (event->key()==Qt::Key_Left){
          if(player->pos().x()>0)
        player->setPos(player->x()-10,player->y());
      }

      else if (event->key()==Qt::Key_Right){
         player->setPos(player->x()+10,player->y());
      }
      else if (event->key()==Qt::Key_Up){
          if(player->pos().y()>0)
        player->setPos(player->x(),player->y()-10);
      }
      else if (event->key()==Qt::Key_Down){
        if((player->pos().y()) < height())
        player->setPos(player->x(),player->y()+10);
      }
    else if(event->key()==Qt::Key_Space){
        player->bubble =new QGraphicsPixmapItem();
        player->bubble->setPixmap(QPixmap(":/bullets/bullets/bubble.png"));
        player->bubble->setScale(0.06);
        player->bubble->setPos(player->x()+130, player->y()+60);
        bubble.insert(player->get_number_of_bubbles(),player->bubble);
        player->in_number_of_bubbles();
        addItem(player->bubble);

        makeSound(bubbleSound);


}

}

void LevelMermaid::update()
{


//ENEMY
    if(timeCounter%15==0){
    EnemyofMermaid *enemy = new EnemyofMermaid();
    addItem(enemy);
    enemies.insert(enemy->ID,enemy);
}
    if(!enemies.isEmpty()){

        for(auto it=enemies.begin(); it!=enemies.end(); it++){
            //pomeramo neprijatelja
           it.value()->setPos(it.value()->x()-10, it.value()->y());

           //provera da li je izasao van granica ekana
           if(it.value()->pos().x()+ 20 <0){
               removeItem(it.value());

               //NOTE:take brise element iz mape i vaca vrednost
               auto value=enemies.take(it.key());
               delete value;
              // qDebug()<<"delete";
           }
           else{
                auto collding_items=this->collidingItems(it.value());
                for( int i=0, n=collding_items.size(); i<n; ++i){
                    if(typeid (*(collding_items[i]))==typeid (PlayerMermaid)){

                            pHealth->decrease();

                            //Ovde proveravam health kako bih videla da li igrac izgubio zivot
                            //ako jeste onda emitujem signal klasi Game da je zivot izgubljen
                            if(pHealth->getHealth() <= 0){

                                // brisanje sadrzaja mapa pre nego sto pozovemo signal
                                // jer se u slotu brise sadrzj scene koji to ne ukljucuje
                                for(auto e=enemies.begin(); e!=enemies.end(); e++){
                                    auto value = enemies.take(e.key());
                                    delete value;
                                }

                                for(auto b=bubble.begin(); b!= bubble.end(); b++){
                                    auto value = bubble.take(b.key());
                                    delete value;
                                }

                               emit lifeLost();

                               return;
                            }

                            this->removeItem(it.value());

                            auto value=enemies.take(it.key());
                            delete value;
                     }
                }
            }

    }

    timeCounter++;
}
    //BUBBLE
        if(!bubble.isEmpty()){
            for(auto it=bubble.begin(); it!=bubble.end(); it++){

                it.value()->setPos(it.value()->x()+10,it.value()->y());

                if(it.value()->pos().x()>width()){
                    removeItem(it.value());
                    auto value=bubble.take(it.key());
                    delete value;
                }
                else{
                    auto collding_items=this->collidingItems(it.value());

                    foreach(QGraphicsItem *item, collding_items){
                      if(typeid (*item)==typeid (EnemyofMermaid)){

                         pScore->increase();
                         this->removeItem(item);
                         this->removeItem(it.value());
                         int id=(qgraphicsitem_cast<EnemyofMermaid *>(item))->ID;
                         auto value1=enemies.take(id);
                         delete value1;
                         auto value2=bubble.take(it.key());
                         delete value2;
                     }
                    }
                }
          }
     }
}

void LevelMermaid::sounds(){


    bubbleSound = new QMediaPlayer();
    bubbleSound->setMedia(QUrl("qrc:/sounds/fire.mp3"));


}

void LevelMermaid::makeSound(QMediaPlayer *music){
    if (music->state() == QMediaPlayer::PlayingState){
               music->setPosition(0);
            }
            else if (music->state() == QMediaPlayer::StoppedState){
                music->play();
            }

}
