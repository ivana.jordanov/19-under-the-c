#ifndef LEVELMERMAID_H
#define LEVELMERMAID_H

#include <Levelscene.h>
#include <PlayerMermaid.h>
#include <QKeyEvent>
#include <QGraphicsPixmapItem>
#include <QMediaPlayer>
#include <EnemyofMermaid.h>
class LevelMermaid: public LevelScene
{
    Q_OBJECT
public:
    LevelMermaid(QObject * parent = nullptr);
    void initialize();

public slots:
    void update();

private:
    void keyPressEvent(QKeyEvent * event);
    void sounds();
    void makeSound(QMediaPlayer *music);

    PlayerMermaid *player;
    QMap<int, QGraphicsPixmapItem*> bubble;
    QMap<int,EnemyofMermaid*> enemies;
    QMediaPlayer * bubbleSound;
    int timeCounter;


};

#endif // LEVELMERMAID_H
