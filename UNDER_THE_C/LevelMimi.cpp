#include "LevelMimi.h"
#include <QDebug>
#include <QKeyEvent>
#include "Key.h"

// WARNING: Ova globalna promenljiva je privremeno tu i bice premestena u privatnu promenljivu klase
unsigned int proba =1;
int bag =0;

LevelMimi::LevelMimi(QObject *parent):
    LevelScene(parent)
{}

void LevelMimi::initialize()
{
    player = new PlayerMimi();
    addItem(player);

    // TODO: Promeniti da item nije fokusiran vec scena
    // setFlag(QGraphicsItem::ItemIsFocusable);
    setFocus(Qt::TabFocusReason);

    //player->setFocus();
    player->setPos(500, 50);

    pScore = new Score();
    addItem(pScore);

    pHealth = new Health();
    pHealth->setPos(pHealth->x(), pHealth->y()+25);
    addItem(pHealth);

    MimisWings=new WingsScore();
    MimisWings->setPos(MimisWings->x()+430, MimisWings->y());
    addItem(MimisWings);

    addImage(new WingsMini(),":/other_photo/wing-clipart-5.png", MimisWings->x()-40,MimisWings->y(), 0.2);

    time=new Timer();
    time->setPos(time->x()+800, time->y());
    addItem(time);

    setBackgroundBrush(QPixmap(":/backgrounds/backgrounds/download.png"));
    // TODO: Videti kako da se scena prilagodjava prozoru
    //       ili staviti veliku scenu na pocetku i prilagoditi view
    setSceneRect(0,0,1000,700);

    speed_items=0;

    proba=0;
    stop=false;
    stopEventKey=false;
    wings=nullptr;
    sounds();


    //inicijalizacija igraca koji treba biti spasen
    koko=new PlayerPlatform();
    addItem(koko);
    koko->setPos(width(), height()+(koko->boundingRect().height())/10);

    cage= new QGraphicsPixmapItem();
    addImage(cage, ":/other_photo/cage-clipart-transparent-background.png",
             width()-30, height()+(koko->boundingRect().height())/10-50,0.6);

    photoStopwatch= new QGraphicsPixmapItem();
    addImage(photoStopwatch,":/other_photo/see-clipart-blank-watch-5.png", photoStopwatch-> x()+780,photoStopwatch->y(), 0.1);

    moveCloud=1;

}

void LevelMimi::update()
{

    if(proba % 10==0){
        Cloud *cloud= new Cloud();
        addItem(cloud);
        clouds.insert(cloud->ID, cloud);

    }

    if(proba%15==0 && !stop){
        Enemy *enemy= new Enemy();
        addItem(enemy);
        enemies.insert(enemy->ID, enemy);

    }

    if(proba % 40==0 && !stop){


        Key *key= new Key();
        addItem(key);
        keys.insert(key->ID,key);

        speed_items=speed_items+0.5;


    }


    if(proba%280==0 && !stop){
        wings=new WingsMini();
        addItem(wings);
    }

    if(proba%20==0){
        time->nextSecond();
    }



// BECKGROUND MUSIC
    if(proba %300==0){
        makeSound(music);
    }

    //CLOUD
    if(!clouds.empty())
        for(auto it=clouds.begin(); it!=clouds.end(); it++){
            it.value()->setPos(it.value()->x(), it.value()->y()-(10+speed_items)*moveCloud);


            if(it.value()->pos().y() + it.value()->height()<0){
                removeItem(it.value());

                auto value=clouds.take(it.key());
                delete value;
            }
        }


   // ENEMY
    if(!enemies.isEmpty()){

        for(auto it=enemies.begin(); it!=enemies.end(); it++){
            //pomeramo neprijatelja
           it.value()->setPos(it.value()->x(), it.value()->y()-10-speed_items);

           //provera da li je izasao van granica ekana
           if(it.value()->pos().y() + it.value()->rect().height()<0){
               removeItem(it.value());

               //NOTE:take brise element iz mape i vaca vrednost
               auto value=enemies.take(it.key());
               delete value;
           }
           else{
                auto collding_items=this->collidingItems(it.value());
                for( int i=0, n=collding_items.size(); i<n; ++i){
                    if(typeid (*(collding_items[i]))==typeid (PlayerMimi)){

                            pHealth->decrease();

                            //Ovde proveravam health kako bih videla da li igrac izgubio zivot
                            //ako jeste onda emitujem signal klasi Game da je zivot izgubljen
                            if(pHealth->getHealth() <= 0){

                                // brisanje sadrzaja mapa pre nego sto pozovemo signal
                                // jer se u slotu brise sadrzj scene koji to ne ukljucuje
                               clean_mem();
                               emit lifeLost();

                               return;
                            }

                            this->removeItem(it.value());

                            auto value=enemies.take(it.key());
                            delete value;
                            break;
                     }
                }
            }
        }
    }

//KEY
    if(!keys.isEmpty())
    for(auto it=keys.begin(); it!=keys.end(); it++){
        //pomeramo kljuceve
       it.value()->setPos(it.value()->x(), it.value()->y()-10-speed_items);

       //provera da li je izasao van granica ekana
       if(it.value()->pos().y() + it.value()->height()<0){

           removeItem(it.value());
           //NOTE:take brise element iz mape i vaca vrednost
           auto value=keys.take(it.key());
           delete value;
       }else{


       //inace proveravamo da li ga je pokupio igrac
       auto collding_items=this->collidingItems(it.value());
       for( int i=0, n=collding_items.size(); i<n; ++i){


           if(typeid (*(collding_items[i]))==typeid (PlayerMimi)){
                   pScore->increase();
                   pScore->increase();
                   this->removeItem(it.value());
                   auto value=keys.take(it.key());
                   delete value;
                   break;

               }


    }
}

}

//FIRE

    if(!fires.isEmpty())
    for(auto it=fires.begin(); it!=fires.end(); it++){
        //pomeramo kljuceve
       it.value()->setPos(it.value()->x(), it.value()->y()+10);

       //provera da li je izasao van granica ekana
       if(it.value()->pos().y() + it.value()->boundingRect().height()*0.2>height()){

           removeItem(it.value());
           //NOTE:take brise element iz mape i vaca vrednost
           auto value=fires.take(it.key());
           delete value;
       }else{


       //inace proveravamo da li se sudario
      auto collding_items=this->collidingItems(it.value());

       foreach(QGraphicsItem *item, collding_items){
         if(typeid (*item)==typeid (Enemy)){

            pScore->increase();
            this->removeItem(item);
            this->removeItem(it.value());
            int id=(qgraphicsitem_cast<Enemy *>(item))->ID;
            if(enemies.find(id)!=enemies.end()){
            auto value1=enemies.take(id);
            delete value1;
            }
            auto value2=fires.take(it.key());
            delete value2;
            break;
            }

         }
       }
    }


    //WINGS

    if(wings){
        wings->setPos(wings->x(), wings->y()-10-speed_items);

        if(wings->pos().y() + wings->height()<0){

            removeItem(wings);
            delete wings;
            wings=nullptr;
        }

        auto collding_items=this->collidingItems(wings);
        for( int i=0, n=collding_items.size(); i<n; ++i){


            if(typeid (*(collding_items[i]))==typeid (PlayerMimi)){
                    MimisWings->inc_number_of_wings();
                    this->removeItem(wings);
                    delete wings;
                    wings=nullptr;

                }


     }


    }



    end_animation(1201);

proba++;
}





void LevelMimi::keyPressEvent(QKeyEvent *event)
{
    if(stopEventKey)
        return;

    if(event->key()==Qt::Key_Left){
        if(player->pos().x()>0)
        player->setPos(player->x()-15, player->y());
    }
    else if(event->key()==Qt::Key_Right){

        if(player->pos().x()<width())
        player->setPos(player->x()+15, player->y());
    }

    else if(event->key()==Qt::Key_Space){
        player->fire = new QGraphicsPixmapItem();
        addImage(player->fire, ":/bullets/fire-clipart-16.png", player->x(),player->y()+70, 0.2);
        fires.insert(player->get_number_of_fires(),player->fire);
        player->in_number_of_fires();



        makeSound(fireSound);

    }else if(event->key()==Qt::Key_C){
        if(MimisWings->use_wings()){
            speed_items=0;
        }

    }

}

void LevelMimi::sounds(){
    music = new QMediaPlayer();
    music->setMedia(QUrl("qrc:/sounds/POL-change-short.wav"));
    music->play();

    fireSound = new QMediaPlayer();
    fireSound->setMedia(QUrl("qrc:/sounds/fire.mp3"));

}


void LevelMimi::makeSound(QMediaPlayer *music){
    if (music->state() == QMediaPlayer::PlayingState){
               music->setPosition(0);
            }
            else if (music->state() == QMediaPlayer::StoppedState){
                music->play();
            }

}


void LevelMimi::clean_mem(){
    for(auto bb=enemies.begin(); bb!=enemies.end(); bb++){
        auto value = enemies.take(bb.key());
        delete value;
    }
    for(auto kk=keys.begin(); kk != keys.end(); kk++){
        auto value = keys.take(kk.key());
        delete value;
    }
    for(auto ff=fires.begin(); ff != fires.end(); ff++){
        auto value = fires.take(ff.key());
        delete value;
    }
    for(auto cc=clouds.begin(); cc != clouds.end(); cc++){
        auto value = clouds.take(cc.key());
        delete value;
    }


    delete music;
}


void LevelMimi::end_animation(unsigned int game_duraton){

    if(proba==game_duraton-50){
        stop=true;

    }

    if(proba>=game_duraton){
        stopEventKey=true;
        if(koko->pos().y()>550){
            koko->setPos(koko->x(),koko-> y()-10);
            cage->setPos(cage->x(), cage->y()-10);
        }


        if(player->pos().y()+(player->boundingRect().height()/10) <height()){
            player->setPos(player->x(),player-> y()+10);

        }
        else if(player->pos().x()< width()-(cage->boundingRect().width())*0.6){
               moveCloud=0;
            player->setPos(player->x()+10, player->y());

        }else if(pScore->getScore()>=100){
                clean_mem();
                emit levelCompleted();
            }
        else{

            clean_mem();
            emit lifeLost();
            }





    }

    /*if(proba==game_duraton+150){

        if(pScore->getScore()>=0){
            clean_mem();
            emit levelCompleted();
        }
    else{

        clean_mem();
        emit lifeLost();
        }
    }*/
}

void LevelMimi::addImage(QGraphicsPixmapItem *item, const QString &fileName, qreal ax, qreal ay, qreal scale)
{
    item->setPixmap(QPixmap(fileName));
    item->setScale(scale);
    item->setPos(ax, ay);
    addItem(item);
}




