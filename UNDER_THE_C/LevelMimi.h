#ifndef LEVELMIMI_H
#define LEVELMIMI_H

#include <Levelscene.h>
#include <QDebug>
#include <PlayerMimi.h>
#include <Enemy.h>
#include <Key.h>
#include "WingsMini.h"
#include "WingsScore.h"
#include <QMediaPlayer>
#include <QMap>
#include "PlayerPlatform.h"
#include "Timer.h"
#include "Cloud.h"

class LevelMimi: public LevelScene
{
    Q_OBJECT
public:
    LevelMimi(QObject *parent = nullptr);

    void initialize();

public slots:
    void update();
private:
    PlayerMimi *player;
    QMap<int, Enemy*> enemies;
    QMap<int, Key*> keys;
    QMap<int, QGraphicsPixmapItem*> fires;
    QMap<int,Cloud*>clouds;
    void keyPressEvent(QKeyEvent * event);
    void sounds();
    void makeSound(QMediaPlayer *music);
    void clean_mem();
    void end_animation(unsigned int game_duraton);
    double speed_items;
    WingsMini *wings;
    WingsScore *MimisWings;
    QMediaPlayer * music;
    QMediaPlayer * fireSound;
    PlayerPlatform *koko;
    bool stop;
    bool stopEventKey;
    QGraphicsPixmapItem *cage;
    QGraphicsPixmapItem *photoStopwatch;
    Timer *time;
    int moveCloud;
    void addImage(QGraphicsPixmapItem* item ,const QString &fileNime, qreal ax=0, qreal ay=0, qreal scale=1);



};

#endif // LEVELMIMI_H
