#include "LevelPinky.h"
#include <QDebug>
#include "Key.h"
// Dodavanje drugih klasa pisati u .h fajl

//WARNING: Globalne promenljive su samo privremene i bice dodate u klasu po potrebi
int radi = 0;

LevelPinky::LevelPinky(QObject *parent):
    LevelScene(parent)
{

}

void LevelPinky::addPlatform(int x, int y, int width, int height){
    Platform* p = new Platform();
    addItem(p);
    p->setPos(x, y);
    p->setRect(0, 0, width, height);

    platforme.push_back(p);
}
//Ovde se pise opis scene i njen STATICNI izlged(velicina scene, raspored platformi...)
void LevelPinky::initialize(){
    setBackgroundBrush(QPixmap(":/backgrounds/sea.png"));
    setSceneRect(0,0,2000,600);

    player = new PlayerPinky();
    addItem(player);
    setFocus(Qt::TabFocusReason);
    player->setFocus();
    player->setPos(500, 50);

    pScore = new Score();
    addItem(pScore);

    pHealth = new Health();
    pHealth->setPos(pHealth->x(), pHealth->y()+25);
    addItem(pHealth);


    addPlatform(550, 390, 200, 40);
    addPlatform(250, 450, 150, 40);
    addPlatform(900, 300, 200, 40);
    addPlatform(1250, 480, 300, 40);
    addPlatform(1250, 200, 50, 40);
    addPlatform(1450, 220, 150, 40);
    addPlatform(20, 180, 180, 40);
    addPlatform(200, 300, 50, 40);
    addPlatform(1700, 400, 200, 40);
    addPlatform(0, 600, 2000, 40);


    speed_items=0;
    radi = 0;
}
//Ovde se pise sta se pomera na tajmer(neprijatelji i slicno)
void LevelPinky::update()
{
    if(radi == 40){
        Enemy *enemy= new Enemy();
        addItem(enemy);
        enemy->setPos(2000, 520);
        Key *key= new Key();
        addItem(key);
        key->setPos(1500, 580);
        keys.insert(key->ID,key);
        enemies.insert(enemy->ID, enemy);
        speed_items=speed_items+0.1;
        radi = 0;
    }
    radi++;
    if(!enemies.isEmpty()){
        for(auto it=enemies.begin(); it!=enemies.end(); it++){
            //pomeramo neprijatelja
           it.value()->setPos(it.value()->x()-10-speed_items, it.value()->y());

           //provera da li je izasao van granica ekana
           if(it.value()->pos().y() + it.value()->rect().height()<0){
               removeItem(it.value());

               //NOTE:take brise element iz mape i vaca vrednost
               auto value=enemies.take(it.key());
               delete value;
           }
           else{
                auto collding_items=this->collidingItems(it.value());
                for( int i=0, n=collding_items.size(); i<n; ++i){
                    if(typeid (*(collding_items[i]))==typeid (PlayerPinky)){

                            pHealth->decrease();

                            //Ovde proveravam health kako bih videla da li igrac izgubio zivot
                            //ako jeste onda emitujem signal klasi Game da je zivot izgubljen
                            if(pHealth->getHealth() <= 0){

                                // brisanje sadrzaja mapa pre nego sto pozovemo signal
                                // jer se u slotu brise sadrzj scene koji to ne ukljucuje
                                for(auto bb=enemies.begin(); bb!=enemies.end(); bb++){
                                    auto value = enemies.take(bb.key());
                                    delete value;
                                }
                                for(auto kk=keys.begin(); kk != keys.end(); kk++){
                                    auto value = keys.take(kk.key());
                                    delete value;
                                }
                                for(auto ff=lianas.begin(); ff != lianas.end(); ff++){
                                    auto value = lianas.take(ff.key());
                                    delete value;
                                }

                               emit lifeLost();

                               return;
                            }

                            this->removeItem(it.value());

                            auto value=enemies.take(it.key());
                            delete value;
                     }
                }
           }
        }
        if(!lianas.isEmpty())
        for(auto it=lianas.begin(); it!=lianas.end(); it++){
            //pomeramo kljuceve
           it.value()->setPos(it.value()->x()+10, it.value()->y());

           //provera da li je izasao van granica ekana
           if(it.value()->pos().x() + it.value()->rect().width()>width()){

               removeItem(it.value());
               //NOTE:take brise element iz mape i vaca vrednost
               auto value=lianas.take(it.key());
               delete value;
           }else{


           //inace proveravamo da li se sudario
          auto collding_items=this->collidingItems(it.value());

           foreach(QGraphicsItem *item, collding_items){
             if(typeid (*item)==typeid (Enemy)){

                pScore->increase();
                this->removeItem(item);
                this->removeItem(it.value());
                int id=(qgraphicsitem_cast<Enemy *>(item))->ID;
                auto value1=enemies.take(id);
                delete value1;
                auto value2=lianas.take(it.key());
                delete value2;


                }
              }
           }
       }
    }

    /*** KRETANJE IGRACA  I KOLIZIJE S PLATFORMAMA ***/

    playerCollide();

    // animacija pada igraca kada ne stoji na platformi i kada nije u skoku
    if(collision_side[3] == 0 && left_right_jump[2] == 0){
        player->setPos(player->x(), player->y() +15);
    }

    // animacija skoka igraca
    if(left_right_jump[2] > 0){
        if(left_right_jump[2] + 1 == 12 || collision_side[2] == 1)
            left_right_jump[2] =0;
        else left_right_jump[2]++;

        player->setPos(player->x(), player->y() -15);
    }

    // Ovaj igrac jos nema slike za smenjivanje prilikom kretanja zato je zakomentarisano

    // smena slika igraca prilikom kretanja na desnu stranu
 /*   if(left_right_jump[1] == 1){

        if(probaa <= 3)
            player->setPixmap(QPixmap(":/slike/players_images/koko/kokoThird.png"));
        else if(probaa > 3 && probaa <= 6)
            player->setPixmap(QPixmap(":/slike/players_images/koko/kokoFourth.png"));
        else if(probaa > 6){
            player->setPixmap(QPixmap(":/slike/players_images/koko/kokoFive.png"));

            if(probaa == 10)
                probaa = 0;
        }

        probaa++;
    }

    // smena slika igraca prilikom kretanja na levu stranu
    if(left_right_jump[0] == 1){

        if(probaa <= 3)
            player->setPixmap(QPixmap(":/slike/players_images/koko/koko_left_third.png"));
        else if(probaa > 3 && probaa <= 6)
            player->setPixmap(QPixmap(":/slike/players_images/koko/koko_left_four.png"));
        else if(probaa > 6){
            player->setPixmap(QPixmap(":/slike/players_images/koko/koko_left_five.png"));

            if(probaa == 10)
                probaa = 1;
        }

        probaa++;
    }

    // ispisivanje slike igraca dok miruje
    if(left_right_jump[0] == 0 && left_right_jump[1] == 0 && probaa > 0){

        // promenljiva last odredjuje da li je poslednje pusten taster za levo ili za desno
        if(last == 1)
            player->setPixmap(QPixmap(":/slike/players_images/koko/koko_left_standing.png"));
        else
            player->setPixmap(QPixmap(":/slike/players_images/koko/kokoD.png"));
        probaa = 0;
    }
*/

    // kretanje igraca na levu stranu uzimajuci u obzir i moguce kolizije s platformama
    if(left_right_jump[0] == 1){
        if(player->pos().x() > 0 && !collision_side[0]){
            player->setPos(player->x() - 14, player->y());

            playerCollide();

            if(collision_side[0])
                player->setPos(player->x() + 14, player->y());
        }
    }
    // kretanje igraca na desnu stranu uzimajuci u obzir moguce kolizije s platformamam
    else if(left_right_jump[1] == 1){
        if(player->pos().x() < width() && !collision_side[1]){
            player->setPos(player->x() + 14, player->y());
            //setSceneRect(x+10, 0, 3000, 1000);
           // x += 10;
            playerCollide();

            if(collision_side[1])
                player->setPos(player->x() -14, player->y());
        }
    }

}
void LevelPinky::keyPressEvent(QKeyEvent *event){

    playerCollide();

    if(event->key()==Qt::Key_Left){
        /*if(player->pos().x()>0)
            player->setPos(player->x()-10, player->y());*/
        left_right_jump[0] = 1;
    }
    else if(event->key()==Qt::Key_Right){
        //800 ces mozda morati da me menjas
        /*if(player->pos().x()<width())
            player->setPos(player->x()+10, player->y());*/
        left_right_jump[1] = 1;
    }
    else if(event->key()==Qt::Key_Up){
        /*if(player->pos().x()<width())
            player->setPos(player->x(), player->y()-10);*/
        if(player->pos().y() > 0 && left_right_jump[2] == 0 && collision_side[3] == 1){
            left_right_jump[2] = 1;
        }
    }
    /*else if(event->key()==Qt::Key_Down){
        if(player->pos().x()<width())
            player->setPos(player->x(), player->y()+10);
    }*/

    else if(event->key()==Qt::Key_Space){
        player->liana = new QGraphicsRectItem();
        player->liana->setRect(0,0,50,50);
        player->liana->setPos(player->x(),player->y()+70);
        lianas.insert(player->get_number_of_lianas(),player->liana);
        player->in_number_of_lianas();
        addItem(player->liana);
    }
}

void LevelPinky::keyReleaseEvent(QKeyEvent *event)
{
    if(event->key() == Qt::Key_Left){
        left_right_jump[0] = 0;
        // kad budu dodate slike za animaciju kretanja ovog igraca bice koriscen ovaj parametar
        //last = 1;
    }
    if(event->key() == Qt::Key_Right){
        left_right_jump[1] = 0;
        //last = 2;
    }
}
void LevelPinky::playerCollide()
{
    auto colliding_items = this->collidingItems(player);
    int n = colliding_items.size();

    collision_side[0] = 0;
    collision_side[1] = 0;
    collision_side[2] = 0;
    collision_side[3] = 0;

    for(int i=0; i < n; ++i){
        if(typeid (*(colliding_items[i])) == typeid (Platform)){

            auto platform = (qgraphicsitem_cast<Platform *>(colliding_items[i]));

            //kolizija igraca i gornje ivice platforme vraca 1 ako je igrac u takvoj koliziji 0 inace
            //(140 - 15 (140 je visina shape za igraca, 15 je preciznost za skok jer se mrda za 15 piksela)
            //skok mora da ide prvi jer inace objekat moze da bude u duploj koliziji s jednom istom platformom kad je na njenoj ivici(zato conitune)
            if(player->pos().y() + 125 <= platform->pos().y()){
                collision_side[3] = 1;
                continue;
            }

            //kolizija igraca i donje ivice platforme vraca 1 ako je igrac u takvoj koliziji 0 inace
            // 10 je dodato radi preciznosti prilikom mrdanja nagore
            if(player->pos().y() +10 >= platform->pos().y() + platform->rect().height())
                collision_side[2] = 1;

            // kolizija igraca i leve ivice platforme, vraca 1 ako je igrac u takvoj koliziji 0 inace
            if(player->pos().x() + 50 <= platform->pos().x())
                collision_side[1] = 1;

            //kolizija igraca i desne ivice platforme, vraca 1 ako je igrac u takvoj koliziji 0 inace +30
            if(player->pos().x() + 30  >= platform->pos().x() + platform->rect().width())
                collision_side[0] = 1;

            //std::cout << platform->pos().y()<< std::endl;


        }
    }
    //std::cout <<"colizija sa desna: "<< collision_side[3] <<" plsyer: " << player->pos().y() << " " << std::endl;
}
