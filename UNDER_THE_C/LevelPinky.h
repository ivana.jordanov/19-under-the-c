#ifndef LEVELPINKY_H
#define LEVELPINKY_H

#include <Levelscene.h>
#include <QKeyEvent>
#include <QDebug>
#include <PlayerPinky.h>
#include <Enemy.h>
#include <Key.h>
//#include <Liana.h>

#include <Platform.h>

#include <QVector>
#include <Platform.h>
#include <QMap>
class LevelPinky: public LevelScene
{
    Q_OBJECT
public:
    LevelPinky(QObject * parent = nullptr);
    void initialize();
public slots:
    void update();
private:
private:
    PlayerPinky *player;
    QMap<int, Enemy*> enemies;
    QMap<int, Key*> keys;
    QMap<int, QGraphicsRectItem*> lianas;
    double speed_items;

    void keyPressEvent(QKeyEvent* event);
    void keyReleaseEvent(QKeyEvent* event);
    void playerCollide();

    int collision_side[4] = {0,0,0,0};
    int left_right_jump[3] = {0,0,0};


    void addPlatform(int x, int y, int width, int height);
    QVector<Platform*> platforme;
};

#endif // LEVELPINKY_H
