#include "LevelPlatform.h"

#include <Platform.h>

#include <iostream>

// privremene globalne promenljive, njihovo ulanjanje se ocekuje u buducnosti
int probaa;
int last;
int x=0;

LevelPlatform::LevelPlatform(QObject *parent):
    LevelScene(parent)
{

};

void LevelPlatform::addPlatform(int x, int y, int width, int height){
    Platform* p = new Platform();
    addItem(p);
    p->setPos(x, y);
    p->setRect(0, 0, width, height);

    platforme.push_back(p);
}
void LevelPlatform::initialize()
{
    setBackgroundBrush(QPixmap(":/backgrounds/backgrounds/sky.jpg"));
    setSceneRect(0,0,2000,500);


    player = new PlayerPlatform();
    addItem(player);
    setFocus(Qt::TabFocusReason);
    player->setFocus();
    player->setPos(500, 50);

    // platforme na sceni koje sluze samo za isprobavanje kretanja igraca i kolizija, najverovatnije nece ostati u dizajnu nivoa
    //NOTE ZA KAJU: slobodno izbrisi ove platforme i menjaj kako ti odgovara
 /*   Platform* p1 = new Platform();
    addItem(p1);
    p1->setPos(457, 403);
    p1->setRect(0,0, 400,300);

    Platform* p2 = new Platform();
    addItem(p2);
    p2->setPos(600, 600);
    p2->setRect(0,0, 500,100);

    Platform* p3 = new Platform();
    addItem(p3);
    p3->setPos(1000, 350);
    p3->setRect(0,0, 200,50);

    Platform* p4 = new Platform();
    addItem(p4);
    p4->setPos(1300, 520);
    p4->setRect(0,0, 400,50);

    Platform* p5 = new Platform();
    addItem(p5);
    p5->setPos(900, 900);
    p5->setRect(0,0, 800,100);
*/

    addPlatform(550, 390, 200, 40);

    addPlatform(250, 450, 150, 40);

    addPlatform(900, 300, 200, 40);

    addPlatform(1250, 480, 300, 40);

    addPlatform(1250, 200, 50, 40);

    addPlatform(1450, 220, 150, 40);

    addPlatform(20, 180, 180, 40);

    addPlatform(200, 300, 50, 40);

    addPlatform(1700, 400, 200, 40);

    addPlatform(0, 600, 2000, 40);

    probaa = 0;
    last = 0;

}
void LevelPlatform::update()
{
    playerCollide();

    // animacija pada igraca kada ne stoji na platformi i kada nije u skoku
    if(collision_side[3] == 0 && left_right_jump[2] == 0){
        player->setPos(player->x(), player->y() +15);
    }

    // animacija skoka igraca
    if(left_right_jump[2] > 0){
        if(left_right_jump[2] + 1 == 12 || collision_side[2] == 1)
            left_right_jump[2] =0;
        else left_right_jump[2]++;

        player->setPos(player->x(), player->y() -15);
    }


    // smena slika igraca prilikom kretanja na desnu stranu
    if(left_right_jump[1] == 1){

        if(probaa <= 3)
            player->setPixmap(QPixmap(":/slike/players_images/koko/kokoThird.png"));
        else if(probaa > 3 && probaa <= 6)
            player->setPixmap(QPixmap(":/slike/players_images/koko/kokoFourth.png"));
        else if(probaa > 6){
            player->setPixmap(QPixmap(":/slike/players_images/koko/kokoFive.png"));

            if(probaa == 10)
                probaa = 0;
        }

        probaa++;
    }

    // smena slika igraca prilikom kretanja na levu stranu
    if(left_right_jump[0] == 1){

        if(probaa <= 3)
            player->setPixmap(QPixmap(":/slike/players_images/koko/koko_left_third.png"));
        else if(probaa > 3 && probaa <= 6)
            player->setPixmap(QPixmap(":/slike/players_images/koko/koko_left_four.png"));
        else if(probaa > 6){
            player->setPixmap(QPixmap(":/slike/players_images/koko/koko_left_five.png"));

            if(probaa == 10)
                probaa = 1;
        }

        probaa++;
    }

    // ispisivanje slike igraca dok miruje
    if(left_right_jump[0] == 0 && left_right_jump[1] == 0 && probaa > 0){

        // promenljiva last odredjuje da li je poslednje pusten taster za levo ili za desno
        if(last == 1)
            player->setPixmap(QPixmap(":/slike/players_images/koko/koko_left_standing.png"));
        else
            player->setPixmap(QPixmap(":/slike/players_images/koko/kokoD.png"));
        probaa = 0;
    }


    // kretanje igraca na levu stranu uzimajuci u obzir i moguce kolizije s platformama
    if(left_right_jump[0] == 1){
        if(player->pos().x() > 0 && !collision_side[0]){
            player->setPos(player->x() - 14, player->y());

            playerCollide();

            if(collision_side[0])
                player->setPos(player->x() + 14, player->y());
        }
    }
    // kretanje igraca na desnu stranu uzimajuci u obzir moguce kolizije s platformamam
    else if(left_right_jump[1] == 1){
        if(player->pos().x() < width() && !collision_side[1]){
            player->setPos(player->x() + 14, player->y());
            //setSceneRect(x+10, 0, 3000, 1000);
           // x += 10;
            playerCollide();

            if(collision_side[1])
                player->setPos(player->x() -14, player->y());
        }
    }

}

void LevelPlatform::keyPressEvent(QKeyEvent *event)
{

    playerCollide();

    if(event->key() == Qt::Key_Left){
        left_right_jump[0] = 1;
    }
    else if(event->key() == Qt::Key_Right){
        left_right_jump[1] = 1;
    }
    else if(event->key() == Qt::Key_Up){
            if(player->pos().y() > 0 && left_right_jump[2] == 0 && collision_side[3] == 1){
                left_right_jump[2] = 1;
            }
    }

}

void LevelPlatform::keyReleaseEvent(QKeyEvent *event)
{
    if(event->key() == Qt::Key_Left){
        left_right_jump[0] = 0;
        last = 1;
    }
    if(event->key() == Qt::Key_Right){
        left_right_jump[1] = 0;
        last = 2;
    }
}

// funkcija koja racuna da li je igrac u koliziji s leve, desne, gornje ili donje ivice bilo kog platforma objekta
void LevelPlatform::playerCollide()
{
    auto colliding_items = this->collidingItems(player);
    int n = colliding_items.size();

    collision_side[0] = 0;
    collision_side[1] = 0;
    collision_side[2] = 0;
    collision_side[3] = 0;

    for(int i=0; i < n; ++i){
        if(typeid (*(colliding_items[i])) == typeid (Platform)){

            auto platform = (qgraphicsitem_cast<Platform *>(colliding_items[i]));

            //kolizija igraca i gornje ivice platforme vraca 1 ako je igrac u takvoj koliziji 0 inace
            //(140 - 15 (140 je visina shape za igraca, 15 je preciznost za skok jer se mrda za 15 piksela)
            //skok mora da ide prvi jer inace objekat moze da bude u duploj koliziji s jednom istom platformom kad je na njenoj ivici(zato conitune)
            if(player->pos().y() + 125 <= platform->pos().y()){
                collision_side[3] = 1;
                continue;
            }

            //kolizija igraca i donje ivice platforme vraca 1 ako je igrac u takvoj koliziji 0 inace
            // 10 je dodato radi preciznosti prilikom mrdanja nagore
            if(player->pos().y() +10 >= platform->pos().y() + platform->rect().height())
                collision_side[2] = 1;

            // kolizija igraca i leve ivice platforme, vraca 1 ako je igrac u takvoj koliziji 0 inace
            if(player->pos().x() + 50 <= platform->pos().x())
                collision_side[1] = 1;

            //kolizija igraca i desne ivice platforme, vraca 1 ako je igrac u takvoj koliziji 0 inace +30
            if(player->pos().x() + 20  >= platform->pos().x() + 350/*platform->rect().width()*/)
                collision_side[0] = 1;

            //std::cout << platform->pos().y()<< std::endl;


        }
    }
    //std::cout <<"colizija sa desna: "<< collision_side[3] <<" plsyer: " << player->pos().y() << " " << std::endl;
}
