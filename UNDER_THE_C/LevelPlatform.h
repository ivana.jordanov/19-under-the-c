#ifndef LEVELPLATFORM_H
#define LEVELPLATFORM_H

#include <Levelscene.h>

#include <QVector>
#include <Platform.h>
#include <PlayerPlatform.h>
#include <QKeyEvent>

class LevelPlatform: public LevelScene
{
    Q_OBJECT
public:
    LevelPlatform(QObject * parent = nullptr);
    void initialize();
public slots:
    void update();
private:
    PlayerPlatform * player;
    void keyPressEvent(QKeyEvent * event);
    void keyReleaseEvent(QKeyEvent* event);
    void playerCollide();
    int collision_side[4] = {0,0,0,0};
    int left_right_jump[3] = {0,0,0};
    void addPlatform(int x, int y, int width, int height);
    QVector<Platform*> platforme;
};

#endif // LEVELPLATFORM_H
