#ifndef LEVELSCENE_H
#define LEVELSCENE_H

#include <QGraphicsScene>

#include "Health.h"
#include "Score.h"
// Mimi treba da nasledi players
//class Player;

class LevelScene: public QGraphicsScene
{
    Q_OBJECT
public:
    LevelScene(QObject *parent = nullptr);
    virtual void initialize();

    int level_type;

public slots:

    void update()
    {}

signals:
    // aktivira se kad igrac izgubi zivot
    void lifeLost();
    // skupljen je zivot
    void lifeCollected();
    // predjen je nivo
    void levelCompleted();

    //void gamePaused();

protected:
   // Player * player;
    Health * pHealth;
    Score * pScore;
};

#endif // LEVELSCENE_H
