#ifndef LIANA_H
#define LIANA_H

#include <QGraphicsRectItem>
#include <QObject>
class Liana:public QObject, public QGraphicsRectItem{
    Q_OBJECT
public:
    static int i;
    Liana();
    static int get_i();
public slots:
    void move();
};

#endif // LIANA_H
