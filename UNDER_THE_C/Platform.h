#ifndef PLATFORM_H
#define PLATFORM_H

#include <QGraphicsRectItem>
#include <QObject>

class Platform: public QObject, public QGraphicsRectItem
{
    Q_OBJECT
public:
    Platform(QObject* parent = nullptr);
};

#endif // PLATFORM_H
