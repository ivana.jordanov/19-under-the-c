#ifndef PLAYERMERMAID_H
#define PLAYERMERMAID_H

#include <QObject>
#include <QGraphicsRectItem>
#include <QKeyEvent>
#include <QGraphicsPixmapItem>
#include <QGraphicsItem>

class PlayerMermaid: public QObject, public QGraphicsPixmapItem{
        Q_OBJECT
public:
    PlayerMermaid(QGraphicsItem *parent = nullptr);
   // void keyPressEvent(QKeyEvent * event);
    QGraphicsPixmapItem *bubble;
    int get_number_of_bubbles();
    void in_number_of_bubbles();
private:
    int number_of_bubbles;
};

#endif // PLAYERMERMAID_H
