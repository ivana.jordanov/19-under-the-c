#ifndef PLAYERMIMI_H
#define PLAYERMIMI_H

#include <QGraphicsRectItem>
#include <QObject>
#include <QGraphicsItem>
#include <QGraphicsPixmapItem>

class PlayerMimi:public QObject, public QGraphicsPixmapItem{
    Q_OBJECT
public:
    PlayerMimi(QGraphicsItem * parent=0);
    //void keyPressEvent(QKeyEvent * event);
    QGraphicsPixmapItem *fire;
    int get_number_of_fires();
    void in_number_of_fires();


private:
    int number_of_fires;



};
#endif // PLAYERMIMI_H
