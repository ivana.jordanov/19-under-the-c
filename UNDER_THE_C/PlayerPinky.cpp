#include "PlayerPinky.h"
#include <QGraphicsScene>

PlayerPinky::PlayerPinky(QGraphicsItem *parent): QGraphicsPixmapItem(parent){

 setPixmap(QPixmap(":/slike/players_images/katarina.png"));
 setScale(0.1);

}

int PlayerPinky::get_number_of_lianas(){
    return number_of_lianas;
}

void PlayerPinky:: in_number_of_lianas(){
    number_of_lianas++;
}
/****************/
QPainterPath PlayerPinky::shape() const
{
    QPainterPath path;
    path.moveTo(0,0);
    path.addRect(200, 0, 400, 1400);
    return path;
}
/*****************/
/*void PlayerPinky::jump()
{
    // do nothing if already jumping
    if(jumping)
        return;

    // start a jump
    startJumping();
}*/

//change "position_type" for the type of yPos
/*bool PlayerPinky::CollisionWithGround(position_type groundYPos)
{
     position_type newYPos = yPos + yVel;
     return newYPos >= groundYPos;
}
*/
