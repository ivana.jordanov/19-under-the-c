#ifndef PLAYERPINKY_H
#define PLAYERPINKY_H

/*#include <QGraphicsRectItem>
#include <QObject>
#include <QGraphicsItem>
#include <QGraphicsPixmapItem>
#include <QKeyEvent>

class PlayerPinky: public QObject, public QGraphicsRectItem{
    Q_OBJECT
public:
    PlayerPinky(QGraphicsItem *parent = nullptr);
    void keyPressEvent(QKeyEvent * event);


};
*/
#include <QGraphicsRectItem>
#include <QObject>
#include <QGraphicsItem>
#include <QGraphicsPixmapItem>

class PlayerPinky:public QObject, public QGraphicsPixmapItem{
    Q_OBJECT
public:
    PlayerPinky(QGraphicsItem * parent=0);
    //void keyPressEvent(QKeyEvent * event);
    QGraphicsRectItem *liana;
    int get_number_of_lianas();
    void in_number_of_lianas();

    /************ Dodato ***************/
    QPainterPath shape() const override;
private:
    int number_of_lianas;

//void jump();
};

#endif // PLAYERPINKY_H
