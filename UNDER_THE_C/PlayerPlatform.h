#ifndef PLAYERPLATFORM_H
#define PLAYERPLATFORM_H

#include <QObject>
#include <QGraphicsRectItem>
#include <QGraphicsPixmapItem>
#include <QPainterPath>

class PlayerPlatform: public QObject, public QGraphicsPixmapItem{
    Q_OBJECT
public:
    PlayerPlatform(QGraphicsItem *parent = nullptr);
    QPainterPath shape() const override;
    //QRectF boundingRect() const override;
};

#endif // PLAYERPLATFORM_H
