#include "Score.h"
#include <QFont>
#include<QFontDatabase>

Score::Score(QGraphicsItem * parent): QGraphicsTextItem(parent){
    score = 0;

    /*setPlainText(QString("Score: ") + QString::number(score));
    setDefaultTextColor(Qt::darkBlue);
    setFont(QFont("times", 16));*/
    int id = QFontDatabase::addApplicationFont(":/font/Lemillion.otf");
      QFontDatabase::applicationFontFamilies(id);
      setPlainText(QString("Score: ") + QString::number(score));
      setDefaultTextColor(Qt::black);
      setFont(QFont("Lemillion",16));
}

void Score::increase(){
    score++;
    setPlainText(QString("Score: ") + QString::number(score));
}

int Score::getScore(){
    return score;
}
