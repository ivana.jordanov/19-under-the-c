QT       += core gui \
        multimedia

greaterThan(QT_MAJOR_VERSION, 4): QT += widgets

CONFIG += c++17

# You can make your code fail to compile if it uses deprecated APIs.
# In order to do so, uncomment the following line.
#DEFINES += QT_DISABLE_DEPRECATED_BEFORE=0x060000    # disables all the APIs deprecated before Qt 6.0.0

SOURCES += \
    Button.cpp \
    Cloud.cpp \
    Enemy.cpp \
    EnemyofMermaid.cpp \
    Game.cpp \
    Health.cpp \
    Key.cpp \
    LevelMermaid.cpp \
    LevelMimi.cpp \
    LevelPinky.cpp \
    LevelPlatform.cpp \
    Levelscene.cpp \
    MenuScene.cpp \
    Platform.cpp \
    Player.cpp \
    PlayerMermaid.cpp \
    PlayerMimi.cpp \
    PlayerPinky.cpp \
    PlayerPlatform.cpp \
    Score.cpp \
    Timer.cpp \
    WingsMini.cpp \
    WingsScore.cpp \
    main.cpp \
    mainwindow.cpp

HEADERS += \
    Button.h \
    Cloud.h \
    Enemy.h \
    EnemyofMermaid.h \
    Game.h \
    Health.h \
    Key.h \
    LevelMermaid.h \
    LevelMimi.h \
    LevelPinky.h \
    LevelPlatform.h \
    Levelscene.h \
    MenuScene.h \
    Platform.h \
    Player.h \
    PlayerMermaid.h \
    PlayerMimi.h \
    PlayerPinky.h \
    PlayerPlatform.h \
    Score.h \
    Timer.h \
    WingsMini.h \
    WingsScore.h \
    mainwindow.h

FORMS += \
    mainwindow.ui

# Default rules for deployment.
qnx: target.path = /tmp/$${TARGET}/bin
else: unix:!android: target.path = /opt/$${TARGET}/bin
!isEmpty(target.path): INSTALLS += target

RESOURCES += \
    resurs.qrc \
    slike.qrc

DISTFILES += \
    ../../project/19-under-the-c/UNDER_THE_C/backgrounds/under the c-pocetna.jpg \
    ../../project/19-under-the-c/UNDER_THE_C/players_images/milana.png \
    other_photo/cage-clipart-transparent-background.png \
    other_photo/see-clipart-blank-watch-5.png

