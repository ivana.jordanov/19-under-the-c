#include "WingsMini.h"
#include <QBrush>
#include <stdlib.h>

WingsMini::WingsMini(QGraphicsItem *parent): QGraphicsPixmapItem(parent)
{
 setPixmap(QPixmap(":/other_photo/wing-clipart-5.png"));
 setScale(0.3);
 int random=rand()%700;
 setPos(random, 700);
}

int WingsMini::height() const{
    return boundingRect().height()*0.3;
}
