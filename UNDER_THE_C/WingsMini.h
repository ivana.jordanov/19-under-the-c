#ifndef MINIWINGS_H
#define MINIWINGS_H
#include <QGraphicsRectItem>

class WingsMini: public QObject, public QGraphicsPixmapItem{
public:
    WingsMini(QGraphicsItem *parent=nullptr);
    int height() const;
};

#endif // MINIWINGS_H
