#include "WingsScore.h"
#include <QFont>
WingsScore::WingsScore(QGraphicsItem * parent):QGraphicsTextItem(parent){

    number_of_wings = 0;

    setPlainText(QString("    ") + QString::number(number_of_wings));
    setDefaultTextColor(Qt::darkBlue);
    setFont(QFont("times", 16));

}

bool WingsScore::use_wings(){
    if(number_of_wings>0){
        number_of_wings--;
        return true;
    }else{
        return false;
    }

     setPlainText(QString("    ") + QString::number(number_of_wings));
}

void WingsScore:: inc_number_of_wings(){
    number_of_wings++;
     setPlainText(QString("    ") + QString::number(number_of_wings));
}
