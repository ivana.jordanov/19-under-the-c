#include "mainwindow.h"
#include <QApplication>

#include <MenuScene.h>

#include <Button.h>
#include <Game.h>
#include <LevelMimi.h>
#include <LevelPlatform.h>
#include <LevelMermaid.h>
int main(int argc, char *argv[])
{
    QApplication a(argc, argv);

    auto game=new Game();
    game->initialize();
    MenuScene *menu_scene = new MenuScene();

    Button *startButton= new Button();
    //promeniti naziv slike
    startButton->initialize(":/buttons/buttons/start.png",0.2, 300,500);
    menu_scene->addItem(startButton);

    Button *exitButton=new Button();
    exitButton->initialize(":/buttons/buttons/exit.png", 0.2, 550,500);
    menu_scene->addItem(exitButton);

    menu_scene->initialize();
    game->view->setScene(menu_scene);
    game->view->show();
    game->view->setHorizontalScrollBarPolicy(Qt::ScrollBarAlwaysOff);
    game->view->setVerticalScrollBarPolicy(Qt::ScrollBarAlwaysOff);

    Button::connect(startButton,SIGNAL(clicked()),game,SLOT(onPlay()));
    Button::connect(exitButton,SIGNAL(clicked()),game,SLOT(onExit()));



    return a.exec();
}
